cookbook-gitlab-runner CHANGELOG
================================

This file is used to list changes made in each version of the cookbook-gitlab-runner cookbook.

0.1.0
-----
- Initial release of cookbook-gitlab-runner
