require 'serverspec'

# Required by serverspec
set :backend, :exec

describe "GitLab runner" do

  describe package('gitlab-runner') do
    it { should be_installed }
  end

  describe command('gitlab-runner --version') do
    its(:stdout) { should match /Version:\s+10\.2\.0/ }
  end

  describe file('/etc/gitlab-runner/config.toml') do
    it { should be_file }
    it { should be_owned_by 'root' }
    it { should be_mode 600 }
  end

  describe file('/etc/gitlab-runner/config.toml') do
    its(:content) { should match /concurrent = 10/ }
    its(:content) { should match /sentry_dsn = "https:\/\/sentry\.localhost\/123"/ }
    its(:content) { should match /\[\[runners]]/ }
    its(:content) { should match /\[runners.docker]/ }
    its(:content) { should match /image = "ruby:2.1"/ }
    its(:content) { should match /privileged = true/ }
    its(:content) { should match /digitalocean-size=2gb/ }
    its(:content) { should match /digitalocean-image=1234/ }
    its(:content) { should match /\[runners\.machine]/ }
    its(:content) { should_not match /MachineOptionsMap/ }
  end

  describe file('/etc/gitlab-runner/cloudinit.sh') do
    it { should be_file }
    it { should be_owned_by 'root' }
    it { should be_mode 700 }
  end

  describe file('/etc/gitlab-runner/cloudinit.sh') do
    its(:content) { should match /#!\/bin\/sh/ }
    its(:content) { should match /\/sbin\/modprobe binfmt_misc/ }
    its(:content) { should match /systemctl daemon-reload/ }
  end
end
