require 'spec_helper'

describe 'cookbook-gitlab-runner::runner_configure' do
  context "when MachineOptionsMap doesn't override MachineOptions" do
    let(:chef_run) do
      ChefSpec::SoloRunner
          .new(role_path: "#{File.dirname(__FILE__)}/roles")
          .converge('role[runner_configure_machine_options_map_simple]')
    end

    it 'changes all MachineOptionsMap to new MachineOptions entries' do
      expect(chef_run).to create_template('/etc/gitlab-runner/config.toml').with(
          owner: 'root',
          group: 'root',
          mode: '0600'
      )

      expect(chef_run).to render_file('/etc/gitlab-runner/config.toml').with_content { |content|
        expect(content).to eq(<<~eos
        concurrent = 1

        [[runners]]
          name = "test-runner-1"
          executor = "docker+machine"


          [runners.docker]
            image = "ruby:2.3"

          [runners.machine]
            MachineName = "machine-%s"
            MachineDriver = "digitalocean"
            MachineOptions = ["boolean-option", "digitalocean-size=2gb", "digitalocean-image=123456", "digitalocean-private-networking=true", "digitalocean-access-token=", "another-boolean-option"]



        eos
                             )
      }
    end
  end

  context 'when Machine is not defined' do
    let(:chef_run) do
      ChefSpec::SoloRunner
          .new(role_path: "#{File.dirname(__FILE__)}/roles")
          .converge('role[runner_configure_no_machine]')
    end

    it 'Machine is not added' do
      expect(chef_run).to create_template('/etc/gitlab-runner/config.toml').with(
          owner: 'root',
          group: 'root',
          mode: '0600'
      )

      expect(chef_run).to render_file('/etc/gitlab-runner/config.toml').with_content { |content|
        expect(content).to eq(<<~eos
        concurrent = 1

        [[runners]]
          name = "test-runner-1"
          executor = "docker+machine"


          [runners.docker]
            image = "ruby:2.3"



         eos
                             )
      }
    end
  end

  context 'when MachineOptionsMap redefines option defined in MachineOptions' do
    let(:chef_run) do
      ChefSpec::SoloRunner
          .new(role_path: "#{File.dirname(__FILE__)}/roles")
          .converge('role[runner_configure_machine_options_overridden]')
    end

    it 'MachineOptions option contains both entries' do
      expect(chef_run).to create_template('/etc/gitlab-runner/config.toml').with(
          owner: 'root',
          group: 'root',
          mode: '0600'
      )

      expect(chef_run).to render_file('/etc/gitlab-runner/config.toml').with_content { |content|
        expect(content).to eq(<<~eos
        concurrent = 1

        [[runners]]
          name = "test-runner-1"
          executor = "docker+machine"


          [runners.docker]
            image = "ruby:2.3"

          [runners.machine]
            MachineName = "machine-%s"
            MachineDriver = "digitalocean"
            MachineOptions = ["digitalocean-size=2gb", "digitalocean-size=1gb"]



         eos
                             )
      }
    end
  end

  context 'when MachineOptionsMaps redefines same option in previous role' do
    let(:chef_run) do
      ChefSpec::SoloRunner
          .new(role_path: "#{File.dirname(__FILE__)}/roles")
          .converge('role[runner_configure_machine_options_map_overridden]')
    end

    it 'MachineOptions contains MachineOptions and the last MachineOptionsMap' do
      expect(chef_run).to create_template('/etc/gitlab-runner/config.toml').with(
          owner: 'root',
          group: 'root',
          mode: '0600'
      )

      expect(chef_run).to render_file('/etc/gitlab-runner/config.toml').with_content { |content|
        expect(content).to eq(<<~eos
        concurrent = 1

        [[runners]]
          name = "test-runner-1"
          executor = "docker+machine"


          [runners.docker]
            image = "ruby:2.3"

          [runners.machine]
            MachineName = "machine-%s"
            MachineDriver = "digitalocean"
            MachineOptions = ["digitalocean-size=2gb", "digitalocean-size=202gb"]



         eos
                             )
      }
    end
  end

  context 'when MachineOptions is not used' do
    let(:chef_run) do
      ChefSpec::SoloRunner
          .new(role_path: "#{File.dirname(__FILE__)}/roles")
          .converge('role[runner_configure_without_machine_options]')
    end

    it 'creates configuration file without errors' do
      expect(chef_run).to create_template('/etc/gitlab-runner/config.toml').with(
          owner: 'root',
          group: 'root',
          mode: '0600'
      )

      expect(chef_run).to render_file('/etc/gitlab-runner/config.toml').with_content { |content|
        expect(content).to eq(<<~eos
        concurrent = 1

        [[runners]]
          name = "test-runner-1"
          executor = "docker+machine"


          [runners.docker]
            image = "ruby:2.3"

          [runners.machine]
            MachineName = "machine-%s"
            MachineDriver = "digitalocean"
            MachineOptions = ["digitalocean-size=2gb"]



                           eos
                           )
      }
    end
  end

  context 'when MachineOptionsMap is not used' do
    let(:chef_run) do
      ChefSpec::SoloRunner
          .new(role_path: "#{File.dirname(__FILE__)}/roles")
          .converge('role[runner_configure_without_machine_options_map]')
    end

    it 'creates configuration file without errors' do
      expect(chef_run).to create_template('/etc/gitlab-runner/config.toml').with(
          owner: 'root',
          group: 'root',
          mode: '0600'
      )

      expect(chef_run).to render_file('/etc/gitlab-runner/config.toml').with_content { |content|
        expect(content).to eq(<<~eos
        concurrent = 1

        [[runners]]
          name = "test-runner-1"
          executor = "docker+machine"


          [runners.docker]
            image = "ruby:2.3"

          [runners.machine]
            MachineName = "machine-%s"
            MachineDriver = "digitalocean"
            MachineOptions = ["digitalocean-size=2gb"]



        eos
                           )
      }
    end
  end

  context 'when no MachineOptions are configured' do
    let(:chef_run) do
      ChefSpec::SoloRunner
          .new(role_path: "#{File.dirname(__FILE__)}/roles")
          .converge('role[runner_configure_no_machine_options]')
    end

    it 'creates configuration file without errors' do
      expect(chef_run).to create_template('/etc/gitlab-runner/config.toml').with(
          owner: 'root',
          group: 'root',
          mode: '0600'
      )

      expect(chef_run).to render_file('/etc/gitlab-runner/config.toml').with_content { |content|
        expect(content).to eq(<<~eos
        concurrent = 1

        [[runners]]
          name = "test-runner-1"
          executor = "docker+machine"


          [runners.docker]
            image = "ruby:2.3"

          [runners.machine]
            MachineName = "machine-%s"
            MachineDriver = "digitalocean"
            MachineOptions = []



                           eos
                           )
      }
    end
  end

  context 'when gcs service account is defined' do
    let(:chef_run) do
      ChefSpec::ServerRunner.new { |node|
        node.normal['cookbook-gitlab-runner'] = {
            'gcp' => {
                'service_account' => {
                    'type' => 'service_account',
                    'client_email' => 'some-user@some-project.iam.gserviceaccount.com',
                    'private_key' => 'key',
                }
            },
            'runners' => {},
        }
      }.converge(described_recipe)
    end

    it 'creates service-account.json file' do
      expect(chef_run).to render_file('/etc/gitlab-runner/service-account.json').with_content { |content|
        expect(content).to eq('{"type":"service_account","client_email":"some-user@some-project.iam.gserviceaccount.com","private_key":"key"}')
      }
    end
  end

  context 'when cache is defined' do
    let(:chef_run) do
      ChefSpec::SoloRunner
          .new(role_path: "#{File.dirname(__FILE__)}/roles")
          .converge('role[runner_configure_cache_options]')
    end

    it 'creates configuration file without errors' do
      expect(chef_run).to create_template('/etc/gitlab-runner/config.toml').with(
          owner: 'root',
          group: 'root',
          mode: '0600'
      )

      expect(chef_run).to render_file('/etc/gitlab-runner/config.toml').with_content {|content|
        expect(content).to eq(<<~eos
        concurrent = 1

        [[runners]]
          name = "test-runner-1"
          executor = "docker+machine"


          [runners.docker]
            image = "ruby:2.3"

          [runners.machine]
            MachineName = "machine-%s"
            MachineDriver = "digitalocean"
            MachineOptions = []


          [runners.cache]
            Type = "gcs"
            Shared = true
            [runners.cache.gcs]
              CredentialsFile = "/etc/gitlab-runner/service-account.json"
              BucketName = "runners-cache"

                         eos
                           )
      }
    end
  end

  context 'when referees is defined' do
    let(:chef_run) do
      ChefSpec::SoloRunner
          .new(role_path: "#{File.dirname(__FILE__)}/roles")
          .converge('role[runner_configure_referees]')
    end

    it 'creates configuration file without errors' do
      expect(chef_run).to create_template('/etc/gitlab-runner/config.toml').with(
          owner: 'root',
          group: 'root',
          mode: '0600'
      )

      expect(chef_run).to render_file('/etc/gitlab-runner/config.toml').with_content {|content|
        expect(content).to eq(<<~eos
        concurrent = 1

        [[runners]]
          name = "test-runner-1"





          [runners.referees]
            [runners.referees.metrics]
              prometheus_address = "http://remote:9090"
              query_interval = 10
              queries = ["arp_entries:rate(node_arp_entries{{selector}}[{interval}])", "context_switches:rate(node_context_switches_total{{selector}}[{interval}])"]
            [runners.referees.network]
              suricata_address = "http://remote:9091"
                         eos
                           )
      }
    end
  end

  context 'when autoscaling is defined' do
    let(:chef_run) do
      ChefSpec::SoloRunner
          .new(role_path: "#{File.dirname(__FILE__)}/roles")
          .converge('role[runner_configure_autoscaling]')
    end

    it 'creates configuration file without errors with autoscaling defined' do
      expect(chef_run).to create_template('/etc/gitlab-runner/config.toml').with(
          owner: 'root',
          group: 'root',
          mode: '0600'
      )

      expect(chef_run).to render_file('/etc/gitlab-runner/config.toml').with_content {|content|
        expect(content).to eq(<<~eos
        concurrent = 1

        [[runners]]
          name = "test-runner-1"
          executor = "docker+machine"


          [runners.docker]
            image = "ruby:2.3"

          [runners.machine]
            MachineName = "machine-%s"
            MachineDriver = "digitalocean"
            MachineOptions = ["digitalocean-size=2gb"]

            [[runners.machine.autoscaling]]
              Periods = ["* 30-59 3-4 * * * *", "* 0-30 4-5 * * * *"]
              IdleCount = 700
              IdleTime = 3600
            [[runners.machine.autoscaling]]
              Periods = ["* * * * * sat,sun *"]
              IdleCount = 70
              IdleTime = 3600
              Timezone = "Europe/Berlin"


                         eos
                           )
      }
    end
  end

  context 'when feature_flags is defined' do
    let(:chef_run) do
      ChefSpec::SoloRunner
          .new(role_path: "#{File.dirname(__FILE__)}/roles")
          .converge('role[runner_configure_feature_flags]')
    end

    it 'creates configuration file without errors with feature flags defined' do
      expect(chef_run).to create_template('/etc/gitlab-runner/config.toml').with(
          owner: 'root',
          group: 'root',
          mode: '0600'
      )

      expect(chef_run).to render_file('/etc/gitlab-runner/config.toml').with_content {|content|
        expect(content).to eq(<<~eos
        concurrent = 1

        [[runners]]
          name = "test-runner-1"
          executor = "docker+machine"
          environment = ["FF_GITLAB_REGISTRY_HELPER_IMAGE=true"]

          [runners.feature_flags]
            FF_SKIP_DOCKER_MACHINE_PROVISION_ON_CREATION_FAILURE = true

          [runners.docker]
            image = "ruby:2.3"



                         eos
                           )
      }
    end
  end
end
